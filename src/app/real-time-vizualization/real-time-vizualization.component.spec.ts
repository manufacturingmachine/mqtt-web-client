import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RealTimeVizualizationComponent } from './real-time-vizualization.component';

describe('RealTimeVizualizationComponent', () => {
  let component: RealTimeVizualizationComponent;
  let fixture: ComponentFixture<RealTimeVizualizationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RealTimeVizualizationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RealTimeVizualizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
