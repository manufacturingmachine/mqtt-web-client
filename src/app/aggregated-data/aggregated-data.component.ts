import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { GraphicsService } from 'src/share/servers/graphics.service';

declare let d3: any;

interface SensorDataPerBatch {
    batch_ID: string,
    endTime: string,
    humidity: number,
    light: number,
    noise: number,
    pressure: number,
    startTime: string,
    temperature: number
}

interface FailureRatePerBatch {
    batch_ID: number,
    nrDamagedComponents: number,
    startTime: Date,
    endTime: Date,
    humidity: number,
    pressure: number,
    temperature: number,
    light: number,
    noise: number,
}

interface FailureRatePerTool {
    tool_ID: number,
    nrDamagedComponents: number,
    startTime: Date,
    endTime: Date,
    humidity: number,
    pressure: number,
    temperature: number,
    light: number,
    noise: number,
}

interface GoodVsFailed {
    errorCode: number,
    nr: number,
    humidity: number,
    pressure: number,
    temperature: number,
    light: number,
    noise: number
}


const enum AggregationType {
    SyncedValues = 'SyncedValues',
    SensorsPerBatch = 'SensorsPerBatch',
    SensorsPerTool = 'SensorsPerTool',
    FailureRatePerBarch = 'FailureRatePerBarch',
    FailureRatePerTool = 'FailureRatePerTool',
    GoodVsFailed = 'GoodVsFailed',
}


@Component({
    selector: 'app-aggregated-data',
    templateUrl: './aggregated-data.component.html',
    styleUrls: ['./aggregated-data.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class AggregatedDataComponent implements OnInit {

    optionsSensorsPerBatch;
    sensorsPerBatch: any;
    sensorsPerTool: any;
    failureRatePerBarch: any;
    failureRatePerTool: any;
    goodVsFailed: any;

    constructor(
        public graphicsService: GraphicsService
    ) { }

    ngOnInit() {
        this.getSensorsPerBatch();
        this.SensorsPerTool();
        this.FailureRatePerBarch();
        this.FailureRatePerTool();
        this.GoodVsFailed();
        this.optionsSensorsPerBatch = {
            chart: {
                type: 'multiBarChart',
                height: 450,
                stacked: true,
            }
        };
    }

    private getSensorData(sensorsDataPerBatch: SensorDataPerBatch[], groupByIdKey: string) {
        const humidity = [];
        const pressure = [];
        const temperature = [];
        const light = [];
        const noise = [];

        for (const sensorDataPerBatch of sensorsDataPerBatch) {
            const id = (<any>sensorDataPerBatch)[groupByIdKey];
            humidity.push({ x: id, y: sensorDataPerBatch.humidity });
            pressure.push({ x: id, y: sensorDataPerBatch.pressure });
            temperature.push({ x: id, y: sensorDataPerBatch.temperature });
            light.push({ x: id, y: sensorDataPerBatch.light });
            noise.push({ x: id, y: sensorDataPerBatch.noise });
        }

        return [
            {
                key: 'Humidity',
                values: humidity
            },
            {
                key: 'Pressure',
                values: pressure
            },
            {
                key: 'Temperature',
                values: temperature
            },
            {
                key: 'Light',
                values: light
            },
            {
                key: 'Noise',
                values: light
            }
        ]
    }

    private getfailureRatePerBatch(failureRatePerBatch: FailureRatePerBatch[], groupByIdKey: string) {
        const nrDamagedComponents = [];

        for (const sensorDataPerBatch of failureRatePerBatch) {
            const id = (<any>sensorDataPerBatch)[groupByIdKey];
            nrDamagedComponents.push({ x: id, y: sensorDataPerBatch.nrDamagedComponents });
        }

        return [
            {
                key: 'nrDamagedComponents',
                values: nrDamagedComponents
            }
        ]
    }

    private getGoodVsFailed(failureRatePerTool: GoodVsFailed[]) {
        const good = [];
        const failedp1 = [];
        const failedp2 = [];
        const failedp1p2 = [];

        for (const sensorDataPerTool of failureRatePerTool) {
            if (sensorDataPerTool.errorCode === null){
                good.push({ x: sensorDataPerTool.errorCode, y: sensorDataPerTool.nr})
            } 
            if (sensorDataPerTool.errorCode === 1){
                failedp1.push({ x: sensorDataPerTool.errorCode, y: sensorDataPerTool.nr })
            } 
            if (sensorDataPerTool.errorCode === 2){
                failedp2.push({ x: sensorDataPerTool.errorCode, y: sensorDataPerTool.nr })
            } 
            if (sensorDataPerTool.errorCode === 3){
                failedp1p2.push({ x: sensorDataPerTool.errorCode, y: sensorDataPerTool.nr })
            } 
        }

        return [
            {
                key: 'Good',
                values: good
            },
            {
                key: 'Failed P1',
                values: failedp1
            },
            {
                key: 'Failed P2',
                values: failedp2
            },
            {
                key: 'Failed P1 & P2',
                values: failedp1p2
            },
        ]
    }

    getSensorsPerBatch() {
        this.graphicsService.getSensorData(AggregationType.SensorsPerBatch).subscribe(response => {
            console.log('response ----> ', response);
            this.sensorsPerBatch = this.getSensorData(response, 'batch_ID');
        });
    }

    SensorsPerTool() {
        this.graphicsService.getSensorData(AggregationType.SensorsPerTool).subscribe(response => {
            console.log('response ----> ', response);
            this.sensorsPerTool = this.getSensorData(response, 'tool_ID');
        });
    }

    FailureRatePerBarch() {
        this.graphicsService.getSensorData(AggregationType.FailureRatePerBarch).subscribe(response => {
            console.log('response ----> ', response);
            this.failureRatePerBarch = this.getfailureRatePerBatch(response, 'batch_ID');
        });
    }

    FailureRatePerTool() {
        this.graphicsService.getSensorData(AggregationType.FailureRatePerTool).subscribe(response => {
            console.log('response ----> ', response);
            this.failureRatePerTool = this.getfailureRatePerBatch(response, 'tool_ID');
        });
    }

    GoodVsFailed() {
        this.graphicsService.getSensorData(AggregationType.GoodVsFailed).subscribe(response => {
            console.log('response ----> ', response);
            this.goodVsFailed = this.getGoodVsFailed(response);
        });
    }

}
