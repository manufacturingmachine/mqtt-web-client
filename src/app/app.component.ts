import { Component, OnInit } from '@angular/core';
// import * as mqtt from 'mqtt';
import * as PahoMQTT from 'paho-mqtt';
import { GraphicsService } from 'src/share/servers/graphics.service';

declare let d3: any;

const enum SensorType {
    Humidity = 'humidity',
    Pressure = 'pressure',
    Temperature = 'temperature',
    Light = 'light',
    Noise = 'noise',
}

interface SensorData {
    title: string;
    data: {
        type: SensorType;
        value: number;
    }[];
    timestamp: number;
}

interface SensorDataPerBatch {
    data: {
        type: SensorType;
        value: number;
    }[];
}

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    title = 'mqtt-web-client';

    options;
    optionsSensorsPerBatch;
    // data: { key: string, values: number[][] }[] = [];
    data: { [key: string]: { key: string, values: number[][] }[] } = {};
    dataSensorsPerBatch: SensorDataPerBatch;
    // private dataEntry: any = {};
    private dataEntry: { [entry: string]: number[][] } = {};

    constructor(
        public graphicsService: GraphicsService
    ) { }

    ngOnInit() {
        this.options = {
            chart: {
                type: 'lineChart',
                useInteractiveGuideline: true,
                // interpolate: 'basis',
                transitionDuration: 350,
                duration: 0,
                height: 250,
                margin: {
                    top: 5,
                    right: 50,
                    bottom: 50,
                    left: 100
                },
                x: function (d) {
                    return d[0];
                },
                y: function (d) {
                    return d[1] / 100;
                },
                xAxis: {
                    axisLabel: 'Timestamp',
                    tickFormat: (d) => {
                        return (new Date(d)).toISOString().replace(/.*T/, '');
                    }
                },
                yAxis: {
                    axisLabel: 'Sensors values',
                    axisLabelDistance: 25,
                    tickFormat: (d) => {
                        return d3.format(',.1%')(d);
                    }
                }
            }
        };

        this.optionsSensorsPerBatch = {
            chart: {
                type: 'multiBarChart',
                height: 450,
                stacked: true,
            }
        };

        // Create a client instance
        const client = new PahoMQTT.Client('localhost', 3000, 'clientId');

        // set callback handlers
        client.onConnectionLost = (responseObject) => {
            if (responseObject.errorCode !== 0) {
                console.log('onConnectionLost:' + responseObject.errorMessage);
            }
        };
        client.onMessageArrived = (message: PahoMQTT.Message) => {
            const sensorsData = JSON.parse(message.payloadString);
            // console.log('onMessageArrived:' + sensorsData);
            this.addSensorsData(sensorsData);
        };

        // connect the client
        client.connect({ onSuccess: onConnect });


        // called when the client connects
        function onConnect() {
            // Once a connection has been made, make a subscription and send a message.
            console.log('onConnect');
            const topic = 'BCDS/XDK110/sensors/data';
            client.subscribe(topic);
            // const message = new PahoMQTT.Message('Hello');
            // message.destinationName = topic;
            // client.send(message);
        }

        // this.getSensorsPerBatch();
    }

    private addSensorsData(sensorsData: SensorData) {
        const timestamp = sensorsData.timestamp = Date.now();
        for (const sensorData of sensorsData.data) {
            const sensorType = sensorData.type;
            if ([/*SensorType.Pressure, SensorType.Light*/].indexOf(sensorType) !== -1) {
                continue;
            }
            let entry = this.dataEntry[sensorType];
            if (!entry) {
                this.dataEntry[sensorType] = entry = [];
                // this.data.push({key: sensorType, values: entry});
                this.data[sensorType] = [{ key: sensorType, values: entry }];
            }
            entry.push([timestamp, sensorData.value]);
            if (entry.length > 50) {
                entry.shift();
            }
            this.data[sensorType] = [...this.data[sensorType]];
            // console.log(this.data);
        }
        // for (let i = 0; i < this.data; i++) {
        //   this.data[i] = [...this.data[i]];
        // }
    }


    private addSensorsPerBatchData(sensorsData: any) {
        console.log('sensorsData ---> ', sensorsData);

        for (const sensorData of sensorsData) {
            const sensorType = sensorData.type;
            if ([/*SensorType.Pressure, SensorType.Light*/].indexOf(sensorType) !== -1) {
                continue;
            }
            let entry = this.dataEntry[sensorType];
            if (!entry) {
                this.dataEntry[sensorType] = entry = [];
                // this.data.push({key: sensorType, values: entry});
                this.data[sensorType] = [{ key: sensorType, values: entry }];
            }

            if (entry.length > 50) {
                entry.shift();
            }
            this.data[sensorType] = [...this.data[sensorType]];
            // console.log(this.data);
        }
        // for (let i = 0; i < this.data; i++) {
        //   this.data[i] = [...this.data[i]];
        // }
    }

    getSensorsPerBatch() {
        // this.graphicsService.getSensorData().subscribe(response => {
        //     console.log('response ----> ', response);
        //     // this.dataSensorsPerBatch.data = response;
        //     // this.addSensorsPerBatchData(this.dataSensorsPerBatch);
        // });
    }
}
