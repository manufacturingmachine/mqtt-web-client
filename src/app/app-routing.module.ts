import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RealTimeVizualizationComponent } from './real-time-vizualization/real-time-vizualization.component';
import { AggregatedDataComponent } from './aggregated-data/aggregated-data.component';

const routes: Routes = [
    { path: '', component: DashboardComponent},
    { path: 'dashboard', component: DashboardComponent },
    { path: 'real-time-vizualization', component: RealTimeVizualizationComponent },
    { path: 'aggregated-data', component: AggregatedDataComponent },
    // { path: 'not-found', loadChildren: './not-found/not-found.module#NotFoundModule' },
    { path: '**', redirectTo: 'not-found' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
