import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class GraphicsService {
    baseUrl = 'http://localhost:4000';

    constructor(
        private http: HttpClient
    ) { }

    getSensorData(param: string): Observable<any> {
        return this.http.get<any[]>(`${this.baseUrl}/aggregate/${param}`);
    }



}
